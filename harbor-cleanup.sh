#!/bin/bash

if [[ ! -x "${HOME}/.local/bin/poetry" ]]; then
  echo "Install poetry see build.sh"
fi
poetry run harbor-cleanup "$@"