# harbor-cleanup

## Useage

* Place a kube config in ~/.kube/config
* Configure: config/config.json
* Build: podman build --network host -t harbor-cleanup .
* Run: podman run -v ${HOME}/.kube/config:/opt/app-root/src/.kube/config:ro --network host --rm -it localhost/harbor-cleanup:latest poetry run harbor-cleanup delete -d

After running the delete check for unwanted deletes
```bash
kubectl get events --all-namespaces --field-selector reason=BackOff,involvedObject.kind=Pod
```

Prometheus Alert Possibilities
* https://xxx/api/v1/namespaces/cattle-monitoring-system/services/http:rancher-monitoring-prometheus:9090/proxy/graph?g0.expr=ALERTS%7Balertname%3D%22KubePodCrashLooping%22%7D&g0.tab=1&g0.stacked=0&g0.show_exemplars=0&g0.range_input=1h
* https://xxx/api/v1/namespaces/cattle-monitoring-system/services/http:rancher-monitoring-prometheus:9090/proxy/graph?g0.expr=ALERTS%7Balertname%3D%22KubePodNotReady%22%7D&g0.tab=1&g0.stacked=0&g0.show_exemplars=0.g0.range_input=1h.

Lists all images

```bash
poetry run harbor-cleanup list
```

```bash
poetry run harbor-cleanup list
```

## Development

* Create a file .env.local

Execute to install the env

```bash
sh build.sh
```

and hit the run button in vs code 