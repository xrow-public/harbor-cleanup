#!/bin/bash

export ENV_FILE=".env.local"

if [[ ! -f $ENV_FILE ]]; then
  echo "To setup your dev environment create a dotenv file (.env.local) with credentials"
  exit 1
fi
rm -Rf list.md
echo "# Image lists" >> list.md
echo "" >> list.md
echo "## Images to keep in registry based on regexp" >> list.md
poetry run harbor-cleanup list-keep >> list.md
echo "" >> list.md
echo "## Images in cluster, but not in registry" >> list.md
poetry run harbor-cleanup list-not-registry >> list.md
echo "" >> list.md
echo "## Images in cluster" >> list.md
poetry run harbor-cleanup list-cluster >> list.md
echo "" >> list.md
echo "## Images delete simulation / final result" >> list.md
poetry run harbor-cleanup delete >> list.md