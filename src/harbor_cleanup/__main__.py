from typing import Type
from kubernetes import client, config
from kubernetes.client import configuration
from kubernetes.client.exceptions import ApiException as KubernetesApiException
from dotenv import load_dotenv
from pathlib import Path
import argparse
import json
import re
import os
import sys
import harbor_client 
from harbor_client.rest import ApiException
import urllib3
urllib3.disable_warnings()
from pprint import pprint
import urllib.parse
from datetime import datetime, timedelta
import logging

app_config = {}
args = any

def run():
   main()
def initLogger():
  global logger
  logger = logging.getLogger()
  logger.setLevel(logging.INFO)
  handler = logging.FileHandler('app.log')
  logger.addHandler(handler)
def main():
  initLogger()
  argParser = argparse.ArgumentParser(
                    prog='Harbor cleanup',
                    description='Cleans up images from harbor',
                    epilog='Text at the bottom of help')
  argParser.add_argument('action', choices=['list-cluster', 'list-keep', 'list-delete', 'list-not-registry', "delete", "help"])
  argParser.add_argument("-c", "--config",type=str , help="Config file", default="config/config.json")
  argParser.add_argument("-k", "--kubernetes-config",type=str , help="Kubernetes config file or env KUBECONFIG", default="~/.kube/config")
  argParser.add_argument("-f", "--force", help="Truely deletes the image for delete action", action='store_true', default=False)
  global args
  args = argParser.parse_args()

  global app_config
  app_config = load_app_config(args.config)
  
  if args.action == "list-cluster":
    action_list_cluster()
  elif args.action == "list-keep":
    action_list_keep()
  elif args.action == "list-delete":
    action_list_delete()
  elif args.action == "list-registry":
    action_list_registry()
  elif args.action == "list-not-registry":
    action_list_not_registry()
  elif args.action == "delete":
    action_delete( args.force )
  else: 
    argParser.print_help()
    return 1
  return 0

def deleteFromRegistry( image, delete=False ):
  global app_config
  api_instance = harbor_client.ArtifactApi(getHarborClient())

  [repository_url, tag] = image.split( ":", 1 )

  repository_name = repository_url.removeprefix( app_config['registry']['url'] + "/" + app_config['registry']['project'] + '/')
  repository_name = quoteString(repository_name)

  try:
    api_response = api_instance.get_artifact( app_config['registry']['project'], repository_name, tag, with_tag=True, with_label=True, with_immutable_status=True)
    timezone = api_response.push_time.tzinfo
    deletebefore = datetime.now(timezone) - timedelta(days=app_config['min-days-after-push'])
    if api_response.push_time > deletebefore:
      return False

    # Rules for retention
    if api_response.labels != None:
      for label in api_response.labels:
        if label.name == "prod":
          return False
    if api_response.tags != None:
      for imagetag in api_response.tags:
        if imagetag.immutable == True:
          return False
    # Delete the specific artifact, or pretend you deleted it
    if delete == True:
      api_response_delete = api_instance.delete_artifact(app_config['registry']['project'], repository_name, tag )
    else:
      return True
    if api_response_delete == None:
      return True
    return True
  except Exception as e:
    print("Exception when deleting " + image + ": %s\n" % e)

def action_delete( delete = False ):
  delete_list = list_delete()
  images_filtered = set(getImagesFromRegistry()) - set(delete_list)
  for image in images_filtered:
    print("[keep] %s" % (image)) 
  for image in delete_list:
    status = deleteFromRegistry(image, delete )
    if status == True:
      print("[deleted] %s" % (image))
    else: 
      print("[keep] %s" % (image))

def action_list_cluster():
  images = getImagesFromClusters()
  print(*images, sep = "\n")

def action_list_not_registry():
  images = getImagesFromClusters()
  global app_config
  reg = re.compile( '^' + app_config['registry']['url'] + '/.*')
  images = list(filter(reg.search, images))
  images = set(getImagesFromClusters()) - set(images)
  print(*images, sep = "\n")

def action_list_registry():
  images = getImagesFromRegistry()
  print(*images, sep = "\n")

def action_list_keep():
  images = getImagesFromRegistry()
  delete = filterImages( images )
  delete = set(delete) - set(getImagesFromClusters())

  images = set(getImagesFromRegistry()) - set(delete)
  print(*images, sep = "\n")

def action_list_delete():
  images = list_delete()
  print(*images, sep = "\n")

def list_delete():
  images = getImagesFromRegistry()
  images = filterImages( images )
  return set(images) - set(getImagesFromClusters())

def getKubernetesClient( context: str ):
  global args
  kubernetes_config = os.environ.get("KUBECONFIG")
  if kubernetes_config != None and os.path.isfile(kubernetes_config):
    config.load_kube_config(kubernetes_config)
  elif args.kubernetes_config != None and os.path.isfile(os.path.expanduser(args.kubernetes_config)):
    config.load_kube_config(args.kubernetes_config)
  else:
    raise Exception("Please define a valid kubeconfig")
  return config.new_client_from_config( context=context )

def getHarborClient( ):
  global app_config
  # Configure HTTP basic authorization: basic
  configuration = harbor_client.Configuration()
  configuration.host = 'https://' + app_config['registry']['url'] + '/api/v2.0'
  configuration.verify_ssl = False
  configuration.username = app_config['registry']['username']
  configuration.password = app_config['registry']['password']
  if os.environ.get("HTTPS_PROXY") != None:
    configuration.proxy = os.environ.get("HTTPS_PROXY")
  if os.environ.get("https_proxy") != None:
    configuration.proxy = os.environ.get("https_proxy")
  # create an instance of the API class
  return harbor_client.ApiClient(configuration)

def getRepositoriesFromRegistry():
  repositories = []
  client = getHarborClient()
  global app_config
  try:
      api_instance = harbor_client.RepositoryApi(client)
      # List repositories
      page = 1
      while True:
        repositories_response = api_instance.list_repositories(app_config['registry']['project'], page=page, page_size=app_config['harbor']['page_size'])
        repositories = repositories + repositories_response
        if len(repositories_response) == 0:
          break
        else:
           page += 1
  except kubernetes.client.exceptions.ApiException as e:
    print("Exception when getting Repositories for " + app_config['registry']['project'] + ": %s\n" % e)
  return repositories

def quoteString( string: str ):
  return urllib.parse.quote( string, safe="" )

def getArtifactsFromRepository( name ):
  global app_config
  artifacts = []
  client = getHarborClient()
  repository_name = quoteString(name)
  try:
      api_instance = harbor_client.ArtifactApi(client)
      # List repositories
      page = 1
      while True:
        artifacts_response = api_instance.list_artifacts(app_config['registry']['project'], repository_name, page=page, page_size=app_config['harbor']['page_size'], with_tag=True, with_label=True, with_immutable_status=True)
        artifacts = artifacts + artifacts_response
        if len(artifacts_response) == 0:
          break
        else:
           page += 1
  except ApiException as e:
    print("Exception for repository " + name + ": %s\n" % e)
  return artifacts

def getImagesFromRegistry():
  images = []
  for repository in getRepositoriesFromRegistry():
    repository_name = repository.name.removeprefix( app_config['registry']['project'] + '/')
    artifacts = getArtifactsFromRepository( repository_name )
    for artifact in artifacts:
      if artifact.type == 'IMAGE' and artifact.tags != None:
        for tag in artifact.tags:
            images.append( app_config['registry']['url'] + "/" + app_config['registry']['project'] + '/' + repository_name + ":" + tag.name)
  return images

def filterImages( images: list ):
  global app_config

  reg = re.compile( '^' + app_config['registry']['url'] + '/.*')
  images = list(filter(reg.search, images))

  # Apply regexp
  for regexp in app_config['regexp']:
    reg = re.compile(regexp)
    keys = list(filter(reg.search, images))
    for key in keys:
      images.remove(key)
  return images

def extractImagesFromSpec( spec ):
  images = []
  if spec.containers:
    for c in spec.containers:
      images.append(c.image)
  if spec.init_containers:
    for c in spec.init_containers:
      images.append(c.image)
  return images

def extractImageFromImageStream( spec ):
  images = []
  if spec.tags:
    for t in spec.tags:
      images.append(getattr(t, 'from'))
  return images

def getImagesFromClusters():
    global app_config
    images = []
    for context in app_config['contexts']:
      images = images + getImagesFromCluster( context )
    return list(dict.fromkeys(images))

def getImagesFromCluster( context: str ):
  # Configs can be set in Configuration class directly or using helper utility
  images = []

  # Just testing for Custom Objects
  # appv1 = client.CustomObjectsApi( getKubernetesClient(context) )
  # try: 
  #   helmcharts = appv1.list_cluster_custom_object( "helm.cattle.io", "v1", "helmcharts")
  #   for i in helmcharts.items():
  #     images = images + extractImageFromImageStream(i.spec)
  # except Exception as e:
  #   global logger
  #   logger.info( "Helmcharts not available in cluster " + context + ": %s\n" % e )
  
  appv1 = client.CustomObjectsApi( getKubernetesClient(context) )
  try: 
    imagestreams = appv1.list_cluster_custom_object( "image.openshift.io", "v1", "imagestreams")
    for i in imagestreams.items():
      images = images + extractImageFromImageStream(i.spec)
  except Exception as e:
    global logger
    logger.info( "Imagestreams not available in cluster " + context + ": %s\n" % e )

  appv1 = client.AppsV1Api( getKubernetesClient(context) )
  deployments = appv1.list_deployment_for_all_namespaces(watch=False)
  for i in deployments.items:
    images = images + extractImagesFromSpec(i.spec.template.spec)

  daemon_sets = appv1.list_daemon_set_for_all_namespaces(watch=False)
  for i in daemon_sets.items:
    images = images + extractImagesFromSpec(i.spec.template.spec)

  stateful_sets = appv1.list_stateful_set_for_all_namespaces(watch=False)
  for i in stateful_sets.items:
    images = images + extractImagesFromSpec(i.spec.template.spec)

  replica_set = appv1.list_replica_set_for_all_namespaces(watch=False)
  for i in replica_set.items:
    images = images + extractImagesFromSpec(i.spec.template.spec)

  batchv1 = client.BatchV1Api( config.new_client_from_config(context=context) )
  cron_jobs = batchv1.list_cron_job_for_all_namespaces(watch=False)
  for i in cron_jobs.items:
    images = images + extractImagesFromSpec(i.spec.job_template.spec.template.spec)

  jobs = batchv1.list_job_for_all_namespaces(watch=False)
  for i in jobs.items:
    images = images + extractImagesFromSpec(i.spec.template.spec)

  v1 = client.CoreV1Api( config.new_client_from_config(context=context) )
  ret = v1.list_pod_for_all_namespaces(watch=False)
  for i in ret.items:
    images = images + extractImagesFromSpec(i.spec)
          


  # Remove doublicates
  return list(dict.fromkeys(images))

def load_app_config(config_file_path: str):
    '''
    load json config file, example:
    {
        "query": {
            "port": 8000,
            "env1": "${ENV1:-default_value}",
            "env2": "${ENV2:-12}"
        }
    }
    :param config_file_path:
    :return:
    '''

    def _cast_to_type(s):
        try:
            return int(s)
        except ValueError:
            try:
                return float(s)
            except ValueError:
                return s

    def _substitude_env_vars(d):
        for key in d.keys():
            v = d.get(key)
            if isinstance(v, str):
                m = re.match('\${([^:}]+)\:-([^:}]*)}', v)
                if m:
                    env_name = m.group(1)
                    def_val = m.group(2)
                    env_val = os.environ.get(env_name)
                    if env_val is None:
                        env_val = _cast_to_type(def_val)
                    d[key] = env_val
            elif isinstance(v, dict):
                _substitude_env_vars(v)
  
    load_dotenv(dotenv_path=Path('.env'), override=False)
    load_dotenv(dotenv_path=Path('.env.local'), override=True)
    path = config_file_path
    if os.path.isfile(path):
        with open(path, 'r') as f:
            app_config = json.load(f)
            _substitude_env_vars(app_config)
            return app_config
    else:
        raise Exception('Configuration file not found: '.format(path))

if __name__ == '__main__':
  sys.exit(main())