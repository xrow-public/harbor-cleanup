#!/bin/bash

export ENV_FILE=".env.local"

if [[ ! -f $ENV_FILE ]]; then
  echo "To setup your dev environment create a dotenv file (.env.local) with credentials"
  exit 1
fi

source <(curl -s -k https://gitlab.com/xrow-public/ci-tools/-/raw/3.0/scripts/library.sh)
ci_dotenv

dnf install -y python python-*
python -m pip install --user pipx
python -m pipx ensurepath
pipx install poetry
poetry install
curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash

helm repo add harbor https://helm.goharbor.io

DOMAIN=${DOMAIN:-dev.xrow.net}
HARBOR_ADMIN_PASSWORD=${HARBOR_ADMIN_PASSWORD:-admin}
HARBOR_REGISTRY=harbor.${DOMAIN}

helm upgrade --install harbor harbor/harbor -n harbor --create-namespace \
  --set expose.ingress.hosts.core=${HARBOR_REGISTRY} \
  --set expose.ingress.hosts.notary=notary.${DOMAIN} \
  --set persistence.enabled=false \
  --set externalURL=https://${HARBOR_REGISTRY}\
  --set harborAdminPassword=${HARBOR_ADMIN_PASSWORD}

podman login ${HARBOR_REGISTRY} --username admin --password ${HARBOR_ADMIN_PASSWORD} --tls-verify=false
podman pull registry.access.redhat.com/ubi9/ubi-minimal:latest
podman pull registry.access.redhat.com/ubi8/ubi-minimal:latest

podman tag registry.access.redhat.com/ubi9/ubi-minimal:latest ${HARBOR_REGISTRY}/eps/test00/ubi-minimal:latest
podman push ${HARBOR_REGISTRY}/eps/test00/ubi-minimal:latest --tls-verify=false --remove-signatures 

podman tag registry.access.redhat.com/ubi8/ubi-minimal:latest ${HARBOR_REGISTRY}/eps/test00/ubi-minimal:delete
podman push ${HARBOR_REGISTRY}/eps/test00/ubi-minimal:delete --tls-verify=false --remove-signatures 

podman tag registry.access.redhat.com/ubi8/ubi-minimal:latest ${HARBOR_REGISTRY}/eps/test01/ubi-minimal:with-prod-label
podman push ${HARBOR_REGISTRY}/eps/test01/ubi-minimal:with-prod-label --tls-verify=false --remove-signatures 

podman tag registry.access.redhat.com/ubi8/ubi-minimal:latest ${HARBOR_REGISTRY}/eps/test02/ubi-minimal:no-delete
podman push ${HARBOR_REGISTRY}/eps/test02/ubi-minimal:no-delete --tls-verify=false --remove-signatures

podman tag registry.access.redhat.com/ubi8/ubi-minimal:latest ${HARBOR_REGISTRY}/eps/test03/ubi-minimal:delete
podman push ${HARBOR_REGISTRY}/eps/test03/ubi-minimal:delete --tls-verify=false --remove-signatures 

helm registry login ${HARBOR_REGISTRY} --username admin --password ${HARBOR_ADMIN_PASSWORD} --insecure
helm pull oci://registry-1.docker.io/bitnamicharts/common --version 2.13.3
helm push common-2.13.3.tgz oci://${HARBOR_REGISTRY}/eps/charts/common --insecure-skip-tls-verify

podman build --network host -t harbor-cleanup .
chmod 644 ${HOME}/.kube/config
podman run -v ${HOME}/.kube/config:/opt/app-root/src/config/kube-config:ro --network host --rm -it \
  -e "KUBECONFIG=config/kube-config" \
  localhost/harbor-cleanup:latest poetry run harbor-cleanup delete
